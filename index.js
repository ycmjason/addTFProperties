const path = require('path');
const fs = require('fs-extra');

const readFile = (path) => fs.readFileSync(path, 'utf8');
const writeFile = (path, s) => fs.writeFileSync(path, s, { flag: 'w+' });
const zipArray = ([xs, ys]) => xs.reduce((acc, x, i) => [...acc, [x, ys[i]]], []);
const createObjectFromEntries = (entries) => entries.reduce((acc, [key, val]) => ({...acc, [key]: val}), {});
const getPropFileName = (ely, lang) => path.join(ely, `src/main/webapp/properties/applicationResources_${lang}.properties`);

const parseCSV = (csv) => {
  const [{ values: langs }, ...props] = csv.split('\n').map(line => line.split('|')).map(([path, ...values]) => ({path, values}));

  return langs.reduce((acc, lang, i) => ({
    ...acc,
    [lang]: props.reduce((acc, {path, values}) => ({
      ...acc,
      [path]: values[i],
    }), {}),
  }), {});
};

const sortInsensitive = (arr) => arr.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));

const insertProps = (origContent, entries) => {
  return sortInsensitive([
    ...origContent.split('\n'),
    ...entries.map(([prop, val]) => `${prop}=${val}`),
  ]).join('\n');
};

const csvEly = process.argv[2];
const csvPath = process.argv[3];

/* {
 *   [lang]: {
 *     [prop]: [val]
 *   }
 * }
 */
const csvParsed = parseCSV(readFile(csvPath).trim());

for (let lang in csvParsed) {
  const propFilePath = getPropFileName(csvEly, lang);
  if(!fs.existsSync(propFilePath)){
    console.log(`${propFilePath} doesn't exist`);
    continue;
  }

  writeFile(propFilePath, insertProps(readFile(propFilePath), Object.entries(csvParsed[lang])).trim());
}
